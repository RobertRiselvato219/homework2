package pm.gui;

import java.io.IOException;
import properties_manager.PropertiesManager;
import saf.ui.AppGUI;
import saf.AppTemplate;
import saf.components.AppWorkspaceComponent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.geometry.Orientation;
import javafx.scene.layout.Pane;
import javafx.scene.layout.HBox;
import javafx.scene.control.Button;
import pm.PropertyType;
import javafx.scene.control.Label;
import javafx.scene.control.SplitPane;
import pm.file.FileManager;
import pm.data.DataManager;
import java.util.ArrayList;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.ColorPicker;
import javafx.scene.paint.Color;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.geometry.Insets;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Rectangle;
import java.util.ArrayList;
import javafx.scene.Cursor;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Shape;
import java.io.File;
import javafx.scene.image.WritableImage;
import javafx.scene.SnapshotParameters;
import javax.imageio.ImageIO;
import javafx.embed.swing.SwingFXUtils;
import saf.ui.AppMessageDialogSingleton;

/**
 * This class serves as the workspace component for this application, providing
 * the user interface controls for editing work.
 *
 * @author Richard McKenna
 * @author Robert Riselvato, ID: 110055510
 * @version 1.0
 */
public class Workspace extends AppWorkspaceComponent {
    
    static final int BUTTON_WIDTH = 35;

    // HERE'S THE APP
    AppTemplate app;

    // IT KNOWS THE GUI IT IS PLACED INSIDE
    AppGUI gui;
    
    FlowPane leftPane;
    Pane rightPane;
    HBox shapeToolbar;
    HBox positionToolbar;
    BorderPane backgroundColor;
    BorderPane fillColor;
    BorderPane outlineColor;
    BorderPane outlineThickness;
    BorderPane snapshot;
    SplitPane workspaceSplitPane;
    Button selectionButton;
    Button removeButton;
    Button rectangleButton;
    Button ellipseButton;
    Button upButton;
    Button downButton;
    Button snapshotButton;
    ArrayList<Button> buttons = new ArrayList<Button>();
    Label backgroundColorLabel;
    Label fillColorLabel;
    Label outlineColorLabel;
    Label outlineThicknessLabel;
    ScrollBar scrollBar;
    ColorPicker backgroundColorPicker;
    ColorPicker outlineColorPicker;
    ColorPicker fillColorPicker;
    boolean released;
    ArrayList<Shape> shapes = new ArrayList<Shape>();
    ArrayList<Double> shapesX = new ArrayList<Double>();
    ArrayList<Double> shapesY = new ArrayList<Double>();
    int i;
    boolean selected = false;
    ArrayList<Integer> numSelected = new ArrayList<Integer>();
    ArrayList<Color> outlineColors = new ArrayList<Color>();
    ArrayList<Color> fillColors = new ArrayList<Color>();
    ArrayList<Double> outlineThicknesses = new ArrayList<Double>();
    Shape selectedShape = new Ellipse();
    File file;
    int fileNum = 1;
    WritableImage snapshotImage = new WritableImage(700, 1000);

    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     *
     * @throws IOException Thrown should there be an error loading application
     * data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
	// KEEP THIS FOR LATER
	app = initApp;

	// KEEP THE GUI FOR LATER
	gui = app.getGUI();
        
        PropertiesManager propsSingleton = PropertiesManager.getPropertiesManager();
        
        workspace = new BorderPane();
        
        leftPane = new FlowPane(Orientation.VERTICAL);
        
        shapeToolbar = new HBox();
        positionToolbar = new HBox();
        backgroundColor = new BorderPane();
        fillColor = new BorderPane();
        outlineColor = new BorderPane();
        outlineThickness = new BorderPane();
        snapshot = new BorderPane();
        workspaceSplitPane = new SplitPane();
        rightPane = new Pane();
        
        FileManager fileManager = (FileManager) app.getFileComponent();
        DataManager dataManager = (DataManager) app.getDataComponent();
        
        new File("./images/snapshots").mkdir();
        file = new File("./images/snapshots/tempImage.png");
        
        selectionButton = gui.initChildButton(shapeToolbar, PropertyType.SELECTION_TOOL_ICON.toString(), PropertyType.SELECTION_TOOL_TOOLTIP.toString(), false);
        selectionButton.setMaxWidth(BUTTON_WIDTH);
        selectionButton.setMinWidth(BUTTON_WIDTH);
        selectionButton.setPrefWidth(BUTTON_WIDTH);
        selectionButton.setPrefHeight(BUTTON_WIDTH);
        Image image = new Image("file:./images/SelectionTool.png");
        selectionButton.setGraphic(new ImageView(image));
        buttons.add(selectionButton);
        selectionButton.setOnAction(e -> {
            rectangleButton.setDisable(false);
            ellipseButton.setDisable(false);
            selectionButton.setDisable(true);
            removeButton.setDisable(true);
            upButton.setDisable(true);
            downButton.setDisable(true);
            rightPane.setCursor(Cursor.DEFAULT);
        });
        
        removeButton = gui.initChildButton(shapeToolbar, PropertyType.REMOVE_ICON.toString(), PropertyType.REMOVE_TOOLTIP.toString(), true);
        removeButton.setMaxWidth(BUTTON_WIDTH);
        removeButton.setMinWidth(BUTTON_WIDTH);
        removeButton.setPrefWidth(BUTTON_WIDTH);
        removeButton.setPrefHeight(BUTTON_WIDTH);
        Image rimage = new Image("file:./images/Remove.png");
        removeButton.setGraphic(new ImageView(rimage));
        buttons.add(removeButton);
        removeButton.setOnAction(e -> {
            int g = shapes.indexOf(selectedShape);
            if (g > -1) {
                shapes.remove(g);
                shapesX.remove(g);
                shapesY.remove(g);
                outlineColors.remove(g);
                outlineThicknesses.remove(g);
                fillColors.remove(g);
                selectedShape = new Ellipse();
                reloadWorkspace();
                removeButton.setDisable(true);
            }
        });
        
        rectangleButton = gui.initChildButton(shapeToolbar, PropertyType.RECTANGLE_ICON.toString(), PropertyType.RECTANGLE_TOOLTIP.toString(), false);
        rectangleButton.setMaxWidth(BUTTON_WIDTH);
        rectangleButton.setMinWidth(BUTTON_WIDTH);
        rectangleButton.setPrefWidth(BUTTON_WIDTH);
        rectangleButton.setPrefHeight(BUTTON_WIDTH);
        Image reimage = new Image("file:./images/Rect.png");
        rectangleButton.setGraphic(new ImageView(reimage));
        buttons.add(rectangleButton);
        rectangleButton.setOnAction(e -> {
            selectionButton.setDisable(false);
            ellipseButton.setDisable(false);
            rectangleButton.setDisable(true);
            numSelected.clear();
            reloadWorkspace();
            removeButton.setDisable(true);
            upButton.setDisable(true);
            downButton.setDisable(true);
            rightPane.setCursor(Cursor.CROSSHAIR);
        });
        
        ellipseButton = gui.initChildButton(shapeToolbar, PropertyType.ELLIPSE_ICON.toString(), PropertyType.ELLIPSE_TOOLTIP.toString(), false);
        ellipseButton.setMaxWidth(BUTTON_WIDTH);
        ellipseButton.setMinWidth(BUTTON_WIDTH);
        ellipseButton.setPrefWidth(BUTTON_WIDTH);
        ellipseButton.setPrefHeight(BUTTON_WIDTH);
        Image eimage = new Image("file:./images/Ellipse.png");
        ellipseButton.setGraphic(new ImageView(eimage));
        buttons.add(ellipseButton);
        ellipseButton.setOnAction(e -> {
            selectionButton.setDisable(false);
            rectangleButton.setDisable(false);
            ellipseButton.setDisable(true);
            numSelected.clear();
            reloadWorkspace();
            removeButton.setDisable(true);
            upButton.setDisable(true);
            downButton.setDisable(true);
            rightPane.setCursor(Cursor.CROSSHAIR);
        });
        
        upButton = new Button();
        upButton.setMaxWidth(BUTTON_WIDTH * 2);
        upButton.setMinWidth(BUTTON_WIDTH * 2);
        upButton.setPrefWidth(BUTTON_WIDTH * 2);
        upButton.setPrefHeight(BUTTON_WIDTH);
        Image uimage = new Image("file:./images/MoveToFront.png");
        upButton.setGraphic(new ImageView(uimage));
        positionToolbar.getChildren().add(upButton);
        buttons.add(upButton);
        upButton.setDisable(true);
        upButton.setOnAction(e -> {
            int t = shapes.indexOf(selectedShape);
            if (t > -1) {
                Shape shape = shapes.get(t + 1);
                double shapeX = shapesX.get(t + 1);
                double shapeY = shapesY.get(t + 1);
                Color outlineColor = outlineColors.get(t + 1);
                double outlineThickness = outlineThicknesses.get(t + 1);
                Color fillColor = fillColors.get(t + 1);
                
                shapes.set(t + 1, selectedShape);
                shapesX.set(t + 1, shapesX.get(t));
                shapesY.set(t + 1, shapesY.get(t));
                outlineColors.set(t + 1, outlineColors.get(t));
                outlineThicknesses.set(t + 1, outlineThicknesses.get(t));
                fillColors.set(t + 1, fillColors.get(t));
                
                shapes.set(t, shape);
                shapesX.set(t, shapeX);
                shapesY.set(t, shapeY);
                outlineColors.set(t, outlineColor);
                outlineThicknesses.set(t, outlineThickness);
                fillColors.set(t, fillColor);
                
                numSelected.add(t + 1);
                reloadWorkspace();
            }
        });
        
        downButton = new Button();
        downButton.setMaxWidth(BUTTON_WIDTH * 2);
        downButton.setMinWidth(BUTTON_WIDTH * 2);
        downButton.setPrefWidth(BUTTON_WIDTH * 2);
        downButton.setPrefHeight(BUTTON_WIDTH);
        Image dimage = new Image("file:./images/MoveToBack.png");
        downButton.setGraphic(new ImageView(dimage));
        positionToolbar.getChildren().add(downButton);
        buttons.add(downButton);
        downButton.setDisable(true);
        downButton.setOnAction(e -> {
            int t = shapes.indexOf(selectedShape);
            if (t > 0) {
                Shape shape = shapes.get(t - 1);
                double shapeX = shapesX.get(t - 1);
                double shapeY = shapesY.get(t - 1);
                Color outlineColor = outlineColors.get(t - 1);
                double outlineThickness = outlineThicknesses.get(t - 1);
                Color fillColor = fillColors.get(t - 1);
                
                shapes.set(t - 1, selectedShape);
                shapesX.set(t - 1, shapesX.get(t));
                shapesY.set(t - 1, shapesY.get(t));
                outlineColors.set(t - 1, outlineColors.get(t));
                outlineThicknesses.set(t - 1, outlineThicknesses.get(t));
                fillColors.set(t - 1, fillColors.get(t));
                
                shapes.set(t, shape);
                shapesX.set(t, shapeX);
                shapesY.set(t, shapeY);
                outlineColors.set(t, outlineColor);
                outlineThicknesses.set(t, outlineThickness);
                fillColors.set(t, fillColor);
                
                numSelected.add(t - 1);
                reloadWorkspace();
            }
        });
        
        backgroundColorLabel = new Label("Background Color");
        backgroundColor.setTop(backgroundColorLabel);
        backgroundColorPicker = new ColorPicker();
        backgroundColor.setBottom(backgroundColorPicker);
        backgroundColorPicker.setOnAction(e -> {
            Color color = backgroundColorPicker.getValue();
            rightPane.setBackground(new Background(new BackgroundFill(color, CornerRadii.EMPTY, Insets.EMPTY)));
        });
        
        fillColorLabel = new Label("Fill Color");
        fillColor.setTop(fillColorLabel);
        fillColorPicker = new ColorPicker();
        fillColor.setBottom(fillColorPicker);
        fillColorPicker.setOnAction(e -> {
            if (selectionButton.isDisabled()) {
                int h = shapes.indexOf(selectedShape);
                fillColors.set(h, fillColorPicker.getValue());
                numSelected.add(h);
                reloadWorkspace();
            }
        });
        
        outlineColorLabel = new Label("Outline Color");
        outlineColor.setTop(outlineColorLabel);
        outlineColorPicker = new ColorPicker();
        outlineColor.setBottom(outlineColorPicker);
        outlineColorPicker.setOnAction(e -> {
            if (selectionButton.isDisabled()) {
                int h = shapes.indexOf(selectedShape);
                outlineColors.set(h, outlineColorPicker.getValue());
                numSelected.add(h);
                reloadWorkspace();
            }
        });
        
        outlineThicknessLabel = new Label("Outline Thickness");
        outlineThickness.setTop(outlineThicknessLabel);
        scrollBar = new ScrollBar();
        outlineThickness.setBottom(scrollBar);
        scrollBar.setOnDragDropped(e -> {
            if (selectionButton.isDisabled()) {
                int h = shapes.indexOf(selectedShape);
                outlineThicknesses.set(h, scrollBar.getValue());
                numSelected.add(h);
                reloadWorkspace();
            }
        });
        
        snapshotButton = new Button();
        snapshotButton.setMaxWidth(BUTTON_WIDTH * 7);
        snapshotButton.setMinWidth(BUTTON_WIDTH * 7);
        snapshotButton.setPrefWidth(BUTTON_WIDTH * 7);
        snapshotButton.setPrefHeight(BUTTON_WIDTH);
        Image simage = new Image("file:./images/Snapshot.png");
        snapshotButton.setGraphic(new ImageView(simage));
        snapshot.setCenter(snapshotButton);
        buttons.add(snapshotButton);
        snapshotButton.setOnAction(e -> {
            File tempFile = new File("./images/snapshots/tempImage" + fileNum + ".png");
            snapshotImage = rightPane.snapshot(new SnapshotParameters(), null);
            try {
                ImageIO.write(SwingFXUtils.fromFXImage(snapshotImage, null), "png", tempFile);
                fileNum++;
                AppMessageDialogSingleton.getSingleton().show("Success", "Picture saved to " + tempFile.getAbsolutePath());
            } catch (IOException fnfe) {}
        });
        
        leftPane = new FlowPane(Orientation.VERTICAL);
        leftPane.getChildren().add(shapeToolbar);
        leftPane.getChildren().add(positionToolbar);
        leftPane.getChildren().add(backgroundColor);
        leftPane.getChildren().add(fillColor);
        leftPane.getChildren().add(outlineColor);
        leftPane.getChildren().add(outlineThickness);
        leftPane.getChildren().add(snapshot);
        
        rightPane = new Pane();
        rightPane.setPrefHeight(1000);
        rightPane.setPrefWidth(700);
        
        leftPane.setPrefHeight(700);
        
        leftPane.setVgap(30);
        
        shapeToolbar.setPrefHeight(BUTTON_WIDTH);
        
        workspaceSplitPane = new SplitPane();
        workspaceSplitPane.getItems().add(leftPane);
        workspaceSplitPane.getItems().add(rightPane);
        
        workspace = new Pane();
        workspace.getChildren().add(workspaceSplitPane);
        
        rightPane.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (rectangleButton.isDisabled()) {
                    Rectangle rectangle = (Rectangle) shapes.get(i);
                    if (event.getX() - shapesX.get(i) > 0) {
                        rectangle.setWidth(event.getX() - shapesX.get(i));   
                    } else if (event.getX() - shapesX.get(i) < 0) {
                        rectangle.setX(event.getX());
                        rectangle.setWidth(shapesX.get(i) - event.getX());
                    }
                    if (event.getY() - shapesY.get(i) > 0) {
                        rectangle.setHeight(event.getY() - shapesY.get(i));
                    } else if (event.getY() - shapesY.get(i) < 0) {
                        rectangle.setY(event.getY());
                        rectangle.setHeight(shapesY.get(i) - event.getY());
                    }
                } else if (ellipseButton.isDisabled()) {
                    Ellipse ellipse = (Ellipse) shapes.get(i);
                    ellipse.setCenterX((event.getX() + shapesX.get(i)) / 2);
                    ellipse.setCenterY((event.getY() + shapesY.get(i)) / 2);
                    ellipse.setRadiusX(Math.abs((event.getX() - shapesX.get(i)) / 2));
                    ellipse.setRadiusY(Math.abs((event.getY() - shapesY.get(i)) / 2));
                }
            }
        });
        /*
        leftPane.setOnMouseClicked(e -> {
            if (selectionButton.isDisabled()) {
                numSelected.clear();
                selectedShape = new Ellipse();
                upButton.setDisable(true);
                downButton.setDisable(true);
                reloadWorkspace();
            }
        });*/
        
        rightPane.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                double posX = event.getX();
                double posY = event.getY();
                if (rectangleButton.isDisabled()) {
                    Rectangle rectangle = new Rectangle(posX, posY, 0, 0);
                    rightPane.getChildren().add(rectangle);
                    rectangle.setFill(fillColorPicker.getValue());
                    fillColors.add(fillColorPicker.getValue());
                    rectangle.setStroke(outlineColorPicker.getValue());
                    outlineColors.add(outlineColorPicker.getValue());
                    rectangle.setStrokeWidth(scrollBar.getValue() / 5);
                    outlineThicknesses.add(scrollBar.getValue() / 5);
                    shapes.add(rectangle);
                    shapesX.add(posX);
                    shapesY.add(posY);
                    rectangle.setOnMouseClicked(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {
                            int k = shapes.indexOf(rectangle);
                            if (selectionButton.isDisable()) {
                                numSelected.add(k);
                                selectionHelper();
                                reloadWorkspace();
                                removeButton.setDisable(false);
                            }
                        }
                    });
                    rectangle.setOnMouseDragged(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {
                            if (selectionButton.isDisabled()) {
                                if (selectedShape instanceof Rectangle) {
                                    Rectangle r = (Rectangle) selectedShape;
                                    if (r.equals(rectangle)) {
                                        r.setX(event.getX());
                                        r.setY(event.getY());
                                    }
                                }
                            }
                        }
                    });
                    i = shapes.size() - 1;
                } else if (ellipseButton.isDisabled()) {
                    Ellipse ellipse = new Ellipse(posX, posY, 0, 0);
                    rightPane.getChildren().add(ellipse);
                    ellipse.setFill(fillColorPicker.getValue());
                    fillColors.add(fillColorPicker.getValue());
                    ellipse.setStroke(outlineColorPicker.getValue());
                    outlineColors.add(outlineColorPicker.getValue());
                    ellipse.setStrokeWidth(scrollBar.getValue() / 5);
                    outlineThicknesses.add(scrollBar.getValue() / 5);
                    shapes.add(ellipse);
                    shapesX.add(posX);
                    shapesY.add(posY);
                    ellipse.setOnMouseClicked(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {
                            int k = shapes.indexOf(ellipse);
                            if (selectionButton.isDisable()) {
                                numSelected.add(k);
                                selectionHelper();
                                reloadWorkspace();
                                removeButton.setDisable(false);
                            }
                        }
                    });
                    ellipse.setOnMouseDragged(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {
                            if (selectionButton.isDisabled()) {
                                if (selectedShape instanceof Ellipse) {
                                    Ellipse e = (Ellipse) selectedShape;
                                    if (e.equals(ellipse)) {
                                        e.setCenterX(event.getX());
                                        e.setCenterY(event.getY());
                                    }
                                }
                            }
                        }
                    });
                    i = shapes.size() - 1;
                }
            }
        });
        
        workspaceActivated = false;
    }
    
    /**
     * This function specifies the CSS style classes for all the UI components
     * known at the time the workspace is initially constructed. Note that the
     * tag editor controls are added and removed dynamicaly as the application
     * runs so they will have their style setup separately.
     */
    @Override
    public void initStyle() {
	// NOTE THAT EACH CLASS SHOULD CORRESPOND TO
	// A STYLE CLASS SPECIFIED IN THIS APPLICATION'S
	// CSS FILE
        shapeToolbar.getStyleClass().add("color_chooser_pane");
        positionToolbar.getStyleClass().add("color_chooser_pane");
        backgroundColor.getStyleClass().add("color_chooser_pane");
        fillColor.getStyleClass().add("color_chooser_pane");
        outlineColor.getStyleClass().add("color_chooser_pane");
        outlineThickness.getStyleClass().add("color_chooser_pane");
        leftPane.getStyleClass().add("max_pane");
        rightPane.getStyleClass().add("color_chooser_pane");
        workspaceSplitPane.getStyleClass().add("max_pane");
        backgroundColorLabel.getStyleClass().add(CLASS_SUBHEADING_LABEL);
        fillColorLabel.getStyleClass().add(CLASS_SUBHEADING_LABEL);
        outlineColorLabel.getStyleClass().add(CLASS_SUBHEADING_LABEL);
        outlineThicknessLabel.getStyleClass().add(CLASS_SUBHEADING_LABEL);
    }

    /**
     * This function reloads all the controls for editing tag attributes into
     * the workspace.
     */
    @Override
    public void reloadWorkspace() {
        if (workspaceActivated) {
            rightPane.getChildren().clear();
            for (Shape shape : shapes) {
                int k = shapes.indexOf(shape);
                shape.setStroke(outlineColors.get(k));
                shape.setStrokeWidth(outlineThicknesses.get(k));
                shape.setFill(fillColors.get(k));
                rightPane.getChildren().add(shape);
            }
            try {
                Shape selectedShape1 = shapes.get(numSelected.get(0));
                selectedShape1.setStroke(Color.YELLOW);
                selectedShape1.setStrokeWidth(25);
                selectedShape = selectedShape1;
                if (shapes.size() == 1) {
                    downButton.setDisable(true);
                    upButton.setDisable(true);
                } else if (shapes.indexOf(selectedShape) == 0) {
                    downButton.setDisable(true);
                    upButton.setDisable(false);
                } else if (shapes.indexOf(selectedShape) == shapes.size() - 1) {
                    downButton.setDisable(false);
                    upButton.setDisable(true);
                } else {
                    downButton.setDisable(false);
                    upButton.setDisable(false);
                }
            } catch (IndexOutOfBoundsException e) {}
            numSelected.clear();
        }
    }
    
    public void selectionHelper() {
        if (numSelected.size() > 1) {
            int max = numSelected.get(0);
            for (int j : numSelected) {
                if (j > max) {
                    max = j;
                }
            }
            numSelected.clear();
            numSelected.add(max);
        }
    }
}
